<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Google Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600;700;800&family=Roboto:wght@300;700&display=swap"
        rel="stylesheet">

    <!-- Boxicons -->
    <link href='https://unpkg.com/boxicons@2.1.0/css/boxicons.min.css' rel='stylesheet'>

    <!-- SwiperJS -->
    <link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />

    <!-- CSS Custom -->
    <link rel="stylesheet" href="css/style.css">

    <title>Techno Red</title>
</head>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://cdn.jsdelivr.net/npm/html5shiv@3.7.3/dist/html5shiv.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/respond.js@1.4.2/dest/respond.min.js"></script>
    <![endif]-->

	<style>
		.background-blue{
    background: #48A8ED;
}

.menu{
    width: 400px;
    float: left;
    height: 70px;
}
:root{
    /* colors */
    --primary-color : #48A8ED;
    --button-color : #FFFFFF;
    --heading-color : #FFFFFF;
    --black-color: #2E2828;

    /* Fonts */
    --main-font : 'Roboto', sans-serif;
    --heading-font : 'Montserrat', sans-serif;
}

*{
    font-family: var(--main-font);
    text-decoration: none;
    list-style: none;
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    transition: .4s ease-in-out;
}

.container {
    max-width: 1420px;
}

.section-margin{
    margin-top: 250px;
}

.heading{
    font-size: 30px;
    font-family: var(--heading-font);
    font-weight: 700;
    color: var(--black-color);
}
.title-section{
    font-size: 16px;
    font-family: var(--main-font);
    font-weight: 700;
    text-transform: uppercase;
    color: var(--primary-color);
}
.bx-x{
    transition: .3s ease;
    transform: rotate(360deg);
}


/* Navbar */
.menu-wrapper .nav-btn-close{
    display: none;
}
.navbar{
    position: fixed;
    top: 0;
    width: 100%;
    left: 50%;
    transform: translateX(-50%);
    height: 140px;
    z-index: 10;
    display: flex;
    align-items: center;
    justify-content: center;
}
.navbar.active{
    background-color: #fff;
    box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.102);
    transition: .4s ease-in-out;
}
.navbar .container{
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;
}
.navbar .nav-brand{
    font-family: var(--heading-font);
    font-size: 30px;
    font-weight: 700;
    color: var(--primary-color);
}
.navbar .nav-brand span{
    font-family: var(--heading-font);
    font-size: 30px;
    font-weight: 700;
    color: var(--heading-color);
}
.navbar .menu{
    display: none;
    font-size: 30px;
    color: var(--primary-color);
}
.navbar .menu-wrapper{
    display: flex;
    justify-content: space-between;
    width: 100%;
    position: relative;
}
.menu-wrapper nav{
    margin: 0 auto;
}
.menu-wrapper .nav-icons i{
    font-size: 25px;
    color: var(--primary-color);

}
.menu-wrapper .nav-icons .icons{
    margin-right: 20px;
}
.menu-wrapper .nav-link{
    margin: 0 25px;
    font-family: var(--heading-font);
    font-size: 18px;
    color: var(--black-color);
    font-weight: 400;
}
.menu-wrapper .nav-link.active{
    font-weight: 700;
    color: var(--primary-color);
}

/* HOME STYLE */
.home .container{
    background-color: var(--primary-color);
    margin: 200px auto 0;
    border-radius: 20px;
    padding: 60px 95px;
    display: flex;
    align-items: center;
}
.home .heading {
    font-family: var(--heading-font);
    font-size: 50px;
    font-weight: 800;
    color: var(--heading-color);
}
.home .subheading{
    font-size: 20px;
    font-weight: 300;
    margin-top: 15px;
    color: #fff;
    width: 460px;
}
.home .btn-home{
    margin-top: 120px;
}
.btn-home .btn-explore{
    background-color: var(--button-color);
    font-family: var(--heading-font);
    font-weight: 600;
    color: var(--primary-color);
    font-size: 18px;
    padding: 16px 24px;
    border-radius: 8px;
    transition: .3s ease-in-out;
}
.btn-home .btn-learn{
    margin-left: 40px;
    padding: 16px 24px;
    border-radius: 8px;
    border: var(--button-color) 2px solid;
    color: var(--button-color);
    font-family: var(--heading-font);
    font-weight: 600;
    transition: .3s ease-in-out;
}
.btn-home .btn-explore:hover{
    background-color: #fff;
    color: var(--primary-color);
    transition: .3s ease-in-out;
}
.btn-home .btn-learn:hover{
    background-color: var(--button-color);
    color: var(--primary-color);
    transition: .3s ease-in-out;
}

.home .content-right{
    max-width: 500px;
}

/* Swiper */
.swiper .swiper-wrapper{
    padding-bottom: 80px;
}

.swiper-slide img{
    display: block;
   margin: auto;
}
.swiper-pagination-bullet{
    width: 12px;
    height: 12px;
    border-radius: 1;
    border: 2px solid #e6dd3b8f;
    background: none;
    opacity: 1;
}
.swiper-pagination-bullet-active{
    background: #E6DD3B;
}

/* Panel Section */
.panel{
    margin-top: 250px;
}
.panel .container{
    margin: auto;
    display: flex;
    justify-content: space-between;
}
.panel .item{
    display: flex;
    align-items: center;
}
.item .icon-item{
    background: #1E6F5C;
    border-radius: 30px 5px;
    padding: 25px;
    display: flex;
    justify-content: center;
    align-items: center;
}
.icon-item i{
    font-size: 50px;
    color: #fff;
}
.item .text-item{
    margin-left: 30px;
}
.text-item .heading-item{
    font-family: var(--heading-font);
    font-weight: 600;
    font-size: 25px;
    color: var(--black-color);
}
.text-item .subheading-item{
    font-weight: 300;
    font-size: 16px;
    color: var(--black-color);
    margin-top: 10px;
}


/* SERVICE SECTION */
.service{
    margin-top: 250px;
}
.service .container{
    margin: auto;
}
.service .row1{
    text-align: center;
}
.service .heading{
    margin-top: 20px;
}
.service .row2{
    margin-top: 100px;
    display: flex;
    justify-content: space-between;
}
.box{
    position: relative;
}
.box .bg-hover{
    background: rgba(30, 111, 92, 0.52);
    backdrop-filter: blur(13px);
    border-radius: 10px;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    clip-path: circle(0.3% at 100% 0);
    transition: .4s ease-in-out;
}
.box:hover .bg-hover{
    clip-path: circle(141.2% at 100% 0);
        transition: .4s ease-in-out;
}
.bg-hover .bg-text{
    font-family: var(--heading-font);
    color: var(--heading-color);
    font-size: 25px;
    font-weight: 800;
}

/* BANNER SECTION */
.banner{
    margin-top: 250px;
}
.banner .container{
    margin: auto;
    display: flex;
    justify-content: space-between;
}
.banner .content-right{
    display: flex;
    flex-wrap: wrap;
    justify-content: end;
    align-items: center;
}

/* PRODUCT SECTION */
.product{
    margin-top: 250px;
}
.product .container{
    margin: auto;
}
.product .row1{
    display: flex;
    justify-content: space-between;
    align-items: center;
}
.product .heading{
    margin-top: 10px;
    width: 736px;
}
.product .btn-slider .bxs-chevron-left-circle, .product .btn-slider .bxs-chevron-right-circle{
    font-size: 50px;
    color: #e6dd3b;
    cursor: pointer;
    margin: 0 5px;
}
.product .row2{
    margin-top: 100px;
}
.product .card-product{
    position: relative;
    background-color: var(--primary-color);
    width: 337px;
    box-shadow: 1px 1px 10px -2px rgba(0, 0, 0, 0.25);
    border-radius: 50px 10px;
    padding: 30px 26px;
}
.card-product img{
    margin: auto;
    display: block;
}
.card-product .detail{
    margin-top: 50px;
}
.card-product .title-card{
    font-family: var(--heading-font);
    font-size: 20px;
    font-weight: 600;
    color: var(--heading-color);
}
.card-product .star{
    margin-top: 10px;
}
.star i{
    font-size: 24px;
    color: #CDEC0D;
}
.detail .stok, .detail .price{
    color: #fff;
}
.detail .price{
    margin-top: 22px;
    font-family: var(--heading-font);
    font-weight: 600;
    font-size: 22px;
}
.card-product .btn-card-product{
    position: absolute;
    right: 0;
    bottom: 0;
    background-color: var(--button-color);
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 22px;
    border-radius: 30px 0px 50px;
}
.card-product .btn-card-product:hover{
    background-color: #fff;
}
.btn-card-product i{
    color: var(--primary-color);
    font-size: 30px;
}
.product .btn-slider .bxs-chevron-left-circle.bxs-chevron-circle-disable, .product .btn-slider .bxs-chevron-right-circle.bxs-chevron-circle-disable{
    color: #e6dd3b80;
}

/* TESTIMONI SECTION */
.testi{
    margin-top: 250px;
}
.testi .container{
    margin: auto;
    display: flex;
    justify-content: space-between;
}
.testi .content-left .text{
    font-size: 20px;
    margin-top: 30px;
    width: 450px;
}
.testi .card-testi{
    display: flex;
    align-items: center;
    background: #48A8ED;
border-radius: 50px 10px;
padding: 20px 40px;
position: relative;
width: 700px;
}
.testi .swiper{
    width: 700px;
}

.card-testi .body-testi{
    width: 460px;
    padding: 50px 40px;
    position: relative;
}
.body-testi .text-testi{
    font-size: 18px;
    color: #fff;
}
.card-testi .body-testi i{
    color: var(--heading-color);
    font-size: 50px;
    position: absolute;
    top: 0;
    left: 0;
}
.card-testi .profile{
    margin-right: 30px;
    text-align: center;
}
.card-testi .profile .name{
    font-family: var(--heading-font);
    font-weight: 700;
    font-size: 20px;
    color: var(--heading-color);
    margin-top: 15px;
}
.card-testi .profile .alamat{
    font-size: 18px;
    color: #fff;
    margin-top: 7px;
}
.card-testi .date{
    font-family: var(--heading-color);
    font-weight: 600;
    font-size: 18px;
    color: rgba(255, 255, 255, 0.685);
    position: absolute;
    top: 20px;
    right: 20px;
}


/* CONTACT SECTION */
.contact{
    margin-top: 250px;
}
.contact .container{
    margin: auto;
    display: flex;
    justify-content: space-between;
}
.contact .heading{
    width: 420px;
}
.contact .content-left img{
    max-width: 500px;
    width: 100%;
    margin-top: 40px;
}
.contact form{
    display: flex;
    flex-direction: column;
}
.contact input{
    width: 700px;
    border: 2px solid var(--primary-color);
}
.contact input:focus, .contact textarea:focus{
    background-color: var(--primary-color);
    color: #fff;
   outline: none;
}
.contact textarea{
    border: 2px solid var(--primary-color);
}

.contact input, .contact textarea, .contact button{
    margin: 10px 0;
    padding: 20px 40px;
    border-radius: 10px;
}
.contact button{
    border: none;
    background-color: var(--button-color);
    font-size: 18px;
    font-family: var(--heading-color);
    font-weight: 600;
    color: var(--primary-color);
    cursor: pointer;
}

/* FOOTER SECTION */    
.footer{
    margin-top: 250px;
    background-color: var(--primary-color);
    border-radius: 50px 50px 0px 0px;
}
.footer .container{
    margin: auto;
}
.footer .row1{
  padding: 90px 0px;
  display: flex;
  justify-content: space-between;
}
.footer .row1 .logo{
    font-family: var(--heading-font);
    font-size: 30px;
    font-weight: 800;
    color: var(--button-color);
}
.footer .row1 .logo span{
    color: var(--heading-color);
}
.footer .row1 .column-left p{
    margin-top: 30px;
    font-size: 20px;
    color: #fff;
}
.column-left .sosmed{
    margin-top: 50px;
}
.sosmed i{
    font-size: 40px;
    color: var(--button-color);
}
.footer .row1 .column-right{
    display: flex;
}
.footer .row1 .column-right .col-3 p{
    font-size: 20px;
    color: #fff;
}
.column-right .col-3 .map{
    display: inline-block;
    margin-top: 20px;
    font-size: 20px;
    color: var(--button-color);
}
.footer .row1 .column-right > div{
    margin: 0 40px;
}
.footer .row1 .column-right h3{
    font-family: var(--heading-font);
    font-size: 20px;
    font-weight: 700;
    color: var(--heading-color);
    margin-bottom: 20px;
}
.row1 .column-right ul li{
    margin-top: 30px;
}
.row1 .column-right ul a{
    font-size: 20px;
    color: #fff;
}
.footer .row2{
    text-align: center;
    padding: 5px 0;
}
.footer .row2 p{
    font-size: 20px;
    color: #fff;
}

/* RESPONSIVE */

/* SMALL DESKTOP */
@media screen and (max-width: 1500px) {
    .navbar, .home, .panel, .service, .banner, .product, .testi, .contact, .footer{
        padding: 0 10px;
    }
    .home .swiper img{
        max-width: 320px;
        width: 100%;
    }
    .home .container{
        margin: 150px auto 0;
        padding: 50px 95px;
    }
    .home .swiper .swiper-wrapper{
        padding-bottom: 50px;
    }
    .home .heading {
        font-size: 46px;
        font-weight: 800;
    }
    .item .icon-item{
        border-radius: 28px 3px;
        padding: 20px;
    }
    .icon-item i{
        font-size: 40px;
    }
    .box img{
        max-width: 320px;
        width: 100%;
    }
    .contact input, .contact textarea, .contact button{
        margin: 10px 0;
        padding: 10px 30px;
        border-radius: 10px;
    }
    .contact .container {
        align-items: center;
    }
    .footer{
        border-radius: 40px 40px 0 0;
    }
}


/* TABLET */
@media screen and (max-width: 768px) {
    .panel, .service, .banner, .product, .testi{
        margin-top: 200px;
    }
    .navbar .menu{
        display: initial;
    }
    .navbar{
        height: 100px;
    }

    .navbar .menu-wrapper{
        background-color: var(--primary-color);
        position: fixed;
        height: 100vh;
        top: 0;
        left: 0;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        text-align: center;
        clip-path: circle(0.3% at 100% 0);
        transition: .4s ease-in-out;
    }
    .navbar .menu-wrapper.active{
        clip-path: circle(141.2% at 100% 0);
        transition: .4s ease-in-out;
    }
    .menu-wrapper .nav-link{
       display: block;
        margin-top: 50px;
        color: #fff;
    }
    .menu-wrapper .nav-link.active{
        color: var(--button-color);
    }
    .menu-wrapper .nav-icons{
        margin-top: 50px;
    }
    .menu-wrapper .nav-btn-close{
        position: absolute;
        top: 50px;
        right: 40px;
        font-size: 40px;
        color: var(--button-color);
        display: initial;
    }
    .menu-wrapper .nav-icons i{
        color: var(--button-color);
    }
    .home .container{
        flex-direction: column;
    }
    .home .content-right{
        margin-top: 70px;
    }
    .home .heading {
        font-size: 46px;
        font-weight: 800;
    }
    .home .subheading{
        font-size: 16px;
    }
    .home .swiper img{
        max-width: 260px;
        width: 100%;
    }
    .panel .container{
        flex-wrap: wrap;
    }
    .panel .item{
        margin-top: 40px;
    }
    .service .row2{
        flex-wrap: wrap;
        justify-content: center;
    }
    .row2 .box{
        margin: 10px;
    }
    .banner .container{
        flex-wrap: wrap;
        justify-content: center;
    }
    .banner .content-right{
        justify-content: center;
    }
    .product .row1{
        flex-wrap: wrap;
        justify-content: end;
    }
    .product .btn-slider{
        margin-top: 20px;
        
    }
    .product img{
        max-width: 150px;
    }
    .detail .price{
        font-size: 18px;
    }
    .card-product .btn-card-product{
        padding: 20px;
        border-radius: 20px 0px 40px;
    }
    .product .card-product{
        border-radius: 40px 10px;
       
    }
    .btn-card-product i{
        font-size: 25px;
    }
    .product .row2{
        margin-top: 60px;
    }
    
    .testi .container{
        flex-wrap: wrap;
    }
    .testi .swiper{
        margin-top: 100px;
    }
    .testi .card-testi{
        display: flex;
        align-items: center;
        background: #1E6F5C;
        border-radius: 50px 10px;
        padding: 20px 40px;
        position: relative;
        width: 600px;
    }
    .testi .swiper{
        width: 600px;
    }
    

    .contact .container{
        flex-wrap: wrap;
        justify-content: center;
    }
    .contact .content-right{
        margin-top: 50px;
    }
    .footer .row1 .column-right{
        flex-wrap: wrap;
    }
}

@media screen and (max-width: 576px) {
    .heading{
        font-size: 26px;
    }
    .panel, .service, .banner{
        margin-top: 150px;
    }
    .navbar{
        height: 80px;
    }
    .home{
        padding: 0 10px;
    }
    .home .container{
        margin: 150px auto 0;
        padding: 40px 45px;
    }
    .home .heading {
        font-size: 36px;
        font-weight: 800;
    }
    .home .subheading{
        font-size: 14px;
        margin-top: 20px;
        width: 100%;
    }
    .btn-home .btn-explore{
        font-size: 12px;
        padding: 16px;
    }
    .btn-home .btn-learn{
        font-size: 12px;
        margin-left: 20px;
        padding: 14px 16px;
    }
    .home .btn-home{
        margin-top: 80px;
    }
    .home .swiper img{
        max-width: 200px;
        width: 100%;
    }
    .item .icon-item{
        border-radius: 30px 5px;
        padding: 20px;
    }
    .icon-item i{
        font-size: 30px;
        color: #fff;
    }
    .item .text-item{
        margin-left: 20px;
    }
    .text-item .heading-item{
        font-size: 20px;
    }
    .service .row1{
        text-align: start;
    }
    .banner .content-left img{
        max-width: 450px;
        width: 100%;
    }
    .banner .content-right img{
        max-width: 550px;
        width: 100%;
    }
    .card-product .title-card{
        font-size: 26px;
    }
    .product .heading{
        margin-top: 10px;
        width: 300px;
    }
    .product .row1{
        flex-wrap: wrap;
        justify-content: space-between;
    }
    .product .swiper-slide{
        width: 300px !important;
    }

    .testi .content-left .text{
        font-size: 16px;
        width: 300px;
    }
    .testi .card-testi{
        flex-wrap: wrap;
    }
    .card-testi .profile img{
        width: 90px;
        height: 90px;
    }
    .card-testi .profile .name{
        font-size: 16px;
    }
    .card-testi .profile .alamat{
        font-size: 14px;
    }
    .testi .swiper{
        margin-top: 50px;
    }

    .contact input{
        width: 100%;
        border: 2px solid var(--primary-color);
    }
    .contact .heading{
        width: 320px;
    }
    .contact .content-left img{
        max-width: 300px;
        width: 100%;
        margin-top: 40px;
    }
    .footer .row1{
        flex-wrap: wrap;
    }
    .footer .row1 .column-right{
        margin-top: 40px;
    }
    .footer .row1 .column-right > div{
        margin: 0;
    }
    .row1 .column-right .col-2{
        margin-left: 30px !important;
    }
    .row1 .column-right .col-3{
        margin-top: 30px !important;
    }
    .footer{
        border-radius: 20px 20px 0 0 ;
    }
}

	</style>
<body>

    <!-- Navbar Start -->
    <header class="header">
        <div class="navbar">
            <div class="container">
                <a href="#" class="nav-brand">Techno Red</a>
                <div class="menu-wrapper">
                    <a href="#" class="nav-btn-close">&times;</a>
                    <nav>
                        <a href="#" class="nav-link active">Home</a>
                        <a href="#" class="nav-link">Klien Kami</a>
                        <a href="#" class="nav-link">Price</a>
                        <a href="#" class="nav-link">Karya</a>
                        <a href="#" class="nav-link">Kontak Kami</a>
                    </nav>
                </div>

                <i class='bx bx-grid-alt menu'></i>
            </div>

        </div>
    </header>
    <!-- Navbar End -->

    
    <section class="home" id="home">
        <div class="container">
            <div class="content-left">
                <img src="image/web.jpg" width="1000"; height="400"; >
                <h1 class="heading">Jasa layanan Pembuatan Web</h1>
                <p class="subheading">Tecno Red Merupakan website yang membuka jasa layanan web dengan berbagai macam jenis website sesuai kebutuhan customer</p>
                 <div class="btn-home">
                 <a href="#" class="btn-learn">Klik selengkapnya tentang web</a>
                <div class="btn-home">
                    <a href="<?= base_url('index.php/controller/login'); ?>" class="page-scroll"><button class="btn-explore" style="background-color: #22CAFF; color:white; border-radius: 8px;">Login</button></a>
                    <a href="#" class="btn-learn">Register</a>
                </div>
            </div>
            <div class="content-right">
               
                
            </div>
        </div>
    </section>

    <!-- Panel Start -->
    <section class="home" id="home">
        <div class="container">
            <div class="item">
                <div class="icon-item">
                    <img src="image/1.png">
                </div>
                <div class="text-item">
                    <h4 class="heading-item">Website e-Commerce</h4>
                    <p class="subheading-item">buat website menarik</p>
                    <p class="subheading-item">fitur lengkap</p>
                    <p class="subheading-item">desain masa kini</p>
                </div>
            </div>
            <div class="item">
                <div class="icon-item">
                    <img src="image/2.png">
                </div>
                <div class="text-item">
                    <h4 class="heading-item">Website Bussiness</h4>
                    <p class="subheading-item">bisnis diminati</p>
                    <p class="subheading-item">tampilan desain menarik</p>
                    <p class="subheading-item">fitur lengkap untuk bisnis</p>
                </div>
            </div>
            <div class="item">
                <div class="icon-item">
                    <img src="image/3.png">
                </div>
                <div class="text-item">
                    <h4 class="heading-item">Website Pendidikan</h4>
                    <p class="subheading-item">diminati pengajar</p>
                    <p class="subheading-item">memudahkan mahasiswa/siswa</p>
                    <p class="subheading-item">tampilan menarik dan mudah dipahami</p>
                </div>
            </div>
        </div>
    </section>
    <!-- Panel End -->

    
    <section class="product">
        <div class="container">
            <div class="row1">
                <div class="title">
                    <p class="title-section">Klien Kami</p>
                </div>
            </div>
            <div class="row1">

                <!-- Swiper -->
                <div class="swiper mySwiperProduct">
                    <div class="swiper-wrapper">
                        <div class="card-product swiper-slide">
                            <img src="image/image.jpg" alt="">
                            <div class="detail">
                                <a href="#" class="title-card">Klien Kami</a>
                            </div>
                        </div>
                        <div class="card-product swiper-slide">
                            <img src="image/image2.jpg" alt="">
                            <div class="detail">
                                <a href="#" class="title-card">Klien Kami</a>
                            </div>
                        </div>
                        <div class="card-product swiper-slide">
                            <img src="image/image3.jpg" alt="">
                            <div class="detail">
                                <a href="#" class="title-card">Klien Kami</a>
                            </div>
                            
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    

    <!-- Contact Start -->
    <!-- Contact End -->

    <!-- Footer Start -->
    <footer class="footer">
        <div class="container">
            <div class="row1">
                <div class="column-left">
                    <a href="#" class="logo">Techno<span>Red</span></a>
                    <p>technored.com</p>
                    <p>HUB: +628529977650</p>
                </div>

                    </div>
                    <div class="col-3">
                        <h3>Address</h3>
                        <p>Jl Lebung Curup <br>
                            No. 123 Blok. H23
                            Rajabasa, <br> Bandar Lampung</p>
                        <a href="#" class="map"><i class='bx bxs-map'></i> Google Map</a>
                    </div>
                </div>
            </div>
            <div class="row2">
                <div class="sosmed">
                        <i class='bx bxl-whatsapp'>0822786823</i>
                        <i class='bx bxl-instagram'>techno</i>
                        <i class='bx bxl-facebook'>technored00</i>
                    </div>
            </div>
        </div>
    </footer>
    <!-- Footer End -->



    <!-- Jquery -->
    <script src="js/jquery-3.6.0.min.js"></script>


    <!-- Swiper JS -->
    <script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>

    <!-- Main JS -->
    <script src="js/script.js"></script>
</body>

</html>